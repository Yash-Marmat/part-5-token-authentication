import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import AlertTemplate from "react-alert-template-basic"; // for the alert box
import Header from "./layout/Header";
import Dashboard from "./leads/Dashboard";
import Alerts from "./layout/Alerts";
import { Provider as AlertProvider } from "react-alert"; // for the alert box
import { Provider } from "react-redux";
import store from "../store";

// Alert Options
const alertOptions = {
  timeout: 3000, // 3 seconds
  position: "top center",
};

class App extends Component {
  render() {
    return (
      <div>
        <Provider store={store}>
          <AlertProvider template={AlertTemplate} {...alertOptions}>
            <Fragment>
              <Header />
              <Alerts />
              <div className="container">
                <Dashboard />
              </div>
            </Fragment>
          </AlertProvider>
        </Provider>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("app"));
